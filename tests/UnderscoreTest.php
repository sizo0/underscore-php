<?php
namespace UnderscoreTest;


use PHPUnit_Framework_TestCase;
use Underscore\Underscore;

class UnderscoreTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var array
     */
    private $array;
    /**
     * @var array
     */
    private $array_key;
    /**
     * @var array
     */
    private $array_array;

    /**
     * @var \Underscore\Underscore
     */
    private $underscore;
    /**
     * @var \Underscore\Underscore
     */
    private $underscore_key;
    /**
     * @var \Underscore\Underscore
     */
    private $underscore_array;
    /**
     * @var \Underscore\Underscore
     */
    private $stooges;

    public function setUp()
    {
        $this->array = [5, 8, 9, 10, 20,];
        $this->array_key = [
            'five' => 5,
            'eight' => 8,
            'nine' => 9,
            'ten' => 10,
            'twenty' => 20,
        ];
        $this->array_array = [$this->array, $this->array_key];

        $this->underscore = new Underscore($this->array);
        $this->underscore_key = new Underscore($this->array_key);
        $this->underscore_array = new Underscore($this->array_array);
        $this->stooges = new Underscore([
            [
                "name" => 'moe',
                "age" => 40,
            ],
            [
                "name" => 'larry',
                "age" => 50,
            ],
            [
                "name" => 'curly',
                "age" => 60,
            ],
        ]);
    }

    public function testConstruct()
    {
        $this->assertInstanceOf('\Underscore\Underscore', $this->underscore);
        $this->assertInstanceOf('\Underscore\Underscore', $this->underscore_key);
        $this->assertInstanceOf('\Underscore\Underscore', $this->underscore_array);
    }

    public function testConstructRecursive()
    {
        $underscore = new Underscore([$this->underscore, $this->underscore_key]);
        $this->assertEquals($underscore, $this->underscore_array);
    }

    public function test_()
    {
        $underscore = Underscore::_();
        $this->assertEquals([], $underscore->toArray());

        $underscore = Underscore::_($this->array);
        $this->assertEquals($this->underscore, $underscore);

        $underscore = Underscore::_($this->array_key);
        $this->assertEquals($this->underscore_key, $underscore);
    }

    public function test_Exception()
    {
        $this->setExpectedException('Exception', "Parameter must be an instance of '\\Underscore\\Underscore', an 'array' or 'null'");
        Underscore::_(2);
    }

    public function testToArray()
    {
        $this->assertEquals($this->array, $this->underscore->toArray());
        $this->assertEquals($this->array_key, $this->underscore_key->toArray());
        $this->assertEquals([
            [
                "name" => 'moe',
                "age" => 40,
            ],
            [
                "name" => 'larry',
                "age" => 50,
            ],
            [
                "name" => 'curly',
                "age" => 60,
            ],
        ], $this->stooges->toArray());
    }

    public function testGetArray()
    {
        $this->assertEquals($this->array, $this->underscore->getArray());
        $this->assertEquals($this->array_key, $this->underscore_key->getArray());
        $this->assertEquals([
            new Underscore([
                "name" => 'moe',
                "age" => 40,
            ]),
            new Underscore([
                "name" => 'larry',
                "age" => 50,
            ]),
            new Underscore([
                "name" => 'curly',
                "age" => 60,
            ]),
        ], $this->stooges->getArray());
    }

    public function testReverse()
    {
        $reversed = $this->underscore->reverse();
        $reversed_preserved = $this->underscore->reverse(true);
        $reversed_key = $this->underscore_key->reverse();

        $this->assertEquals(new Underscore([20, 10, 9, 8, 5,]), $reversed);
        $this->assertEquals(new Underscore([
            4 => 20,
            3 => 10,
            2 => 9,
            1 => 8,
            0 => 5,
        ]), $reversed_preserved);
        $this->assertEquals(new Underscore([
            'twenty' => 20,
            'ten' => 10,
            'nine' => 9,
            'eight' => 8,
            'five' => 5,
        ]), $reversed_key);
    }

    public function testPush()
    {
        $underscore = $this->underscore;
        $underscore->push(2);

        $array = $this->array;
        $array[] = 2;

        $this->assertEquals($array, $underscore->toArray());
    }

    public function testPushWithKey()
    {
        $underscore = $this->underscore;
        $underscore->push('key', 2);

        $array = $this->array;
        $array['key'] = 2;

        $this->assertEquals($array, $underscore->toArray());
    }

    public function testEach()
    {
        $iterate = 0;
        $this->underscore->each(function () use (&$iterate) {
            $iterate++;
        });

        $this->assertEquals(count($this->array), $iterate);
    }

    public function testEachBreak()
    {
        $iterate = 0;
        $this->underscore->each(function ($value) use (&$iterate) {
            $iterate++;
            if ($value == 10) {
                return true;
            }
        });

        $this->assertEquals(4, $iterate);
    }

    public function testMap()
    {
        $underscore = $this->underscore->map(function ($value) {
            return $value * $value;
        });
        $underscore_key = $this->underscore_key->map(function ($value) {
            return $value * $value;
        });

        $this->assertEquals(new Underscore([25, 64, 81, 100, 400,]), $underscore);
        $this->assertEquals(new Underscore([
            'five' => 25,
            'eight' => 64,
            'nine' => 81,
            'ten' => 100,
            'twenty' => 400,
        ]), $underscore_key);
    }

    public function testFoldLeft()
    {
        $fold = $this->underscore->fold_left(function ($acc, $value) {
            return $acc + $value;
        }, 0);
        $this->assertEquals(52, $fold);

        $fold = $this->underscore->fold_left(function ($acc, $value) {
            $acc[] = $value;
            return $acc;
        }, []);
        $this->assertEquals($this->array, $fold);
    }

    public function testFoldRight()
    {
        $fold = $this->underscore->fold_right(function ($acc, $value) {
            return $acc + $value;
        }, 0);
        $this->assertEquals(52, $fold);

        $fold = $this->underscore->fold_right(function ($acc, $value) {
            $acc[] = $value;
            return $acc;
        }, []);
        $this->assertEquals(array_reverse($this->array), $fold);
    }

    public function testFind()
    {
        $iterate = 0;
        $found = $this->underscore->find(function ($value) use (&$iterate) {
            $iterate++;
            return $value % 2 == 0;
        });

        $this->assertEquals(2, $iterate);
        $this->assertEquals(8, $found);
    }

    public function testFilter()
    {
        $filtered = $this->underscore->filter(function ($value) use (&$iterate) {
            $iterate++;
            return $value % 2 == 0;
        });

        $this->assertEquals(count($this->array), $iterate);
        $this->assertEquals([8, 10, 20,], $filtered->toArray());
    }

    public function testContains()
    {
        $contains = $this->underscore->contains([5, 10, 8,]);
        $this->assertTrue($contains);

        $contains = $this->underscore_key->contains(['five' => 5, 8,]);
        $this->assertTrue($contains);
    }

    public function testContainsOneValue()
    {
        $contains = $this->underscore->contains(5);
        $this->assertTrue($contains);

        $contains = $this->underscore->contains(6);
        $this->assertFalse($contains);
    }

    public function testContainsUsingKeys()
    {
        $contains = $this->underscore->contains([5, 10, 8,], true);
        $this->assertFalse($contains);

        $contains = $this->underscore_key->contains(['five' => 5, 8,], true);
        $this->assertFalse($contains);

        $contains = $this->underscore->contains([5, 8, 9,], true);
        $this->assertTrue($contains);

        $contains = $this->underscore_key->contains(['five' => 5, 'eight' => 8,], true);
        $this->assertTrue($contains);
    }

    public function testWhere()
    {
        $where = $this->underscore_array->where(['nine' => 9, 'twenty' => 20]);
        $this->assertEquals([$this->underscore_key->toArray()], $where->toArray());

        $where = $this->underscore_array->where([0 => 5]);
        $this->assertEquals([$this->underscore->toArray()], $where->toArray());

        $where = $this->underscore_array->where([0 => 8]);
        $this->assertEquals(Underscore::_()->toArray(), $where->toArray());
    }

    public function testWhereException()
    {
        $this->setExpectedException('Exception', "Parameter must be an instance of '\\Underscore\\Underscore', an 'array' or 'null'");
        $this->underscore->where(['nine' => 9, 'twenty' => 20]);
    }

    public function testFindWhere()
    {
        $findWhere = $this->underscore_array->findWhere(['nine' => 9, 'twenty' => 20]);
        $this->assertEquals($this->underscore_key, $findWhere);

        $findWhere = $this->underscore_array->findWhere([0 => 5]);
        $this->assertEquals($this->underscore, $findWhere);

        $findWhere = $this->underscore_array->findWhere([0 => 8]);
        $this->assertNull($findWhere);
    }

    public function testFindWhereException()
    {
        $this->setExpectedException('Exception', "Parameter must be an instance of '\\Underscore\\Underscore', an 'array' or 'null'");
        $this->underscore->findWhere(['nine' => 9, 'twenty' => 20]);
    }

    public function testReject()
    {
        $rejected = $this->underscore->reject(function ($value) use (&$iterate) {
            $iterate++;
            return $value % 2 == 0;
        });

        $this->assertEquals(count($this->array), $iterate);
        $this->assertEquals([5, 9,], $rejected->toArray());
    }

    public function testEveryTrue()
    {
        $every = $this->underscore->every(function ($value) use (&$iterate) {
            $iterate++;
            return is_numeric($value);
        });

        $this->assertEquals(count($this->array), $iterate);
        $this->assertTrue($every);
    }

    public function testEveryFalse()
    {
        $every = $this->underscore->every(function ($value) use (&$iterate) {
            $iterate++;
            return $value % 2 == 0;
        });

        $this->assertLessThan(count($this->array), $iterate);
        $this->assertFalse($every);
    }

    public function testSomeTrue()
    {
        $some = $this->underscore->some(function ($value) use (&$iterate) {
            $iterate++;
            return $value % 2 == 0;
        });

        $this->assertLessThan(count($this->array), $iterate);
        $this->assertTrue($some);
    }

    public function testSomeFalse()
    {
        $some = $this->underscore->some(function ($value) use (&$iterate) {
            $iterate++;
            return is_null($value);
        });

        $this->assertEquals(count($this->array), $iterate);
        $this->assertFalse($some);
    }

    public function testGet()
    {
        $value = $this->underscore->get(2);
        $this->assertEquals(9, $value);
    }

    public function testGetException()
    {
        $this->setExpectedException('Exception', "Parameter must a 'string' or an 'integer'");
        $this->underscore->get([]);
    }

    public function testFirst1Value()
    {
        $first = $this->underscore->first();
        $this->assertEquals(5, $first);
    }

    public function testFirst()
    {
        $first = $this->underscore->first(4);
        $this->assertEquals([5, 8, 9, 10,], $first->toArray());
    }

    public function testSize()
    {
        $size = $this->underscore->size();
        $this->assertEquals(5, $size);
    }

    public function testPluck()
    {
        $underscore = new Underscore([$this->array_key]);
        $pluck = $underscore->pluck('five');

        $this->assertEquals(new Underscore([5]), $pluck);
    }

    public function testPluckException()
    {
        $underscore = new Underscore([$this->array_key]);
        $this->setExpectedException('Exception', "Parameter must a 'string' or an 'integer'");
        $underscore->pluck([]);
    }

    public function testMax()
    {
        $max = $this->underscore->max();
        $this->assertEquals(20, $max);
    }

    public function testMaxCallback()
    {
        $max = $this->stooges->max(function (Underscore $o) {
            return $o->get('age');
        });
        $this->assertEquals(["name" => 'curly', "age" => 60,], $max->toArray());
    }

    public function testMin()
    {
        $min = $this->underscore->min();
        $this->assertEquals(5, $min);
    }

    public function testMinCallback()
    {
        $min = $this->stooges->min(function (Underscore $o) {
            return $o->get('age');
        });
        $this->assertEquals(["name" => 'moe', "age" => 40,], $min->toArray());
    }

    public function testSortByCallable()
    {
        $sorted = $this->underscore->sortBy(function ($value) {
            return cos($value);
        });
        $this->assertEquals([9, 10, 8, 5, 20,], $sorted->toArray());
        $this->assertNotEquals($sorted, $this->underscore);
    }

    public function testSortByString()
    {
        $sorted = $this->stooges->sortBy('name');
        $this->assertEquals([
            [
                "name" => 'curly',
                "age" => 60,
            ],
            [
                "name" => 'larry',
                "age" => 50,
            ],
            [
                "name" => 'moe',
                "age" => 40,
            ],
        ], $sorted->toArray());
        $this->assertNotEquals($sorted, $this->stooges);
    }

    public function testSortByException()
    {
        $this->setExpectedException("Exception", "The parameter must be a 'string' or a 'callable'");
        $this->stooges->sortBy(1);
    }

    public function testGroupBy()
    {
        $groupBy = $this->underscore->groupBy(function ($value) {
            return (int)floor($value / 10);
        });
        $this->assertEquals([[5, 8, 9,], [10,], [20,],], $groupBy->toArray());
    }

    public function testIndexBy()
    {
        $indexed = $this->stooges->indexBy(function (Underscore $value) {
            return $value->get('age');
        });
        $this->assertEquals([
            40 => [
                "name" => 'moe',
                "age" => 40,
            ],
            50 => [
                "name" => 'larry',
                "age" => 50,
            ],
            60 => [
                "name" => 'curly',
                "age" => 60,
            ],
        ], $indexed->toArray());
    }

    public function countBy()
    {
        $count = $this->underscore->countBy(function ($value) {
            return $value % 2 == 0 ? 'even' : 'odd';
        });
        $this->assertEquals([
            "odd" => 2,
            "even" => 3,
        ], $count->toArray());
    }

    public function testShuffle()
    {
        $shuffle = $this->underscore->shuffle();
        $this->assertNotEquals($this->underscore->toArray(), $shuffle->toArray());
    }

    public function testSampleWithoutParameter()
    {
        $sample = $this->underscore->sample();
        $this->assertEquals(1, $sample->size());
    }

    public function testSample()
    {
        $sample = $this->underscore->sample(4);
        $this->assertEquals(4, $sample->size());
    }

    public function testSampleLTSize()
    {
        $sample = $this->underscore->sample(-1);
        $this->assertEquals(1, $sample->size());
    }

    public function testSampleGTSize()
    {
        $sample = $this->underscore->sample(7);
        $this->assertEquals(5, $sample->size());
    }

    public function testSampleException()
    {
        $this->setExpectedException("Exception", "Parameter must be an 'integer'");
        $this->underscore->sample('string');
    }

    public function testPartition()
    {
        $partiton = $this->underscore->partition(function ($value) {
            return $value % 2 == 0;
        });
        $this->assertEquals([[8, 10, 20,], [5, 9,]], $partiton->toArray());
    }

    public function testInitialWithoutParameter()
    {
        $initial = $this->underscore->initial();
        $this->assertEquals([5, 8, 9, 10,], $initial->toArray());
    }

    public function testInitial()
    {
        $initial = $this->underscore->initial(2);
        $this->assertEquals([5, 8, 9,], $initial->toArray());
    }

    public function testInitialLTSize()
    {
        $initial = $this->underscore->initial(-1);
        $this->assertEquals([5, 8, 9, 10,], $initial->toArray());
    }

    public function testInitialGTSize()
    {
        $initial = $this->underscore->initial(7);
        $this->assertEquals([], $initial->toArray());
    }

    public function testLast1Value()
    {
        $last = $this->underscore->last();
        $this->assertEquals(20, $last);
    }

    public function testLast()
    {
        $last = $this->underscore->last(3);
        $this->assertEquals([9, 10, 20,], $last->toArray());
    }

    public function testRest()
    {
        $rest = $this->underscore->rest(2);
        $this->assertEquals([9, 10, 20,], $rest->toArray());
    }

    public function testRestLTSize()
    {
        $rest = $this->underscore->rest(-1);
        $this->assertEquals([20,], $rest->toArray());
    }

    public function testRestGTSize()
    {
        $rest = $this->underscore->rest(7);
        $this->assertEquals([], $rest->toArray());
    }

    public function testCompact()
    {
        $compact = (new Underscore([0, 1, false, 2, "", 3,]))->compact();
        $this->assertEquals([1, 2, 3,], $compact->toArray());
    }

    public function testCompactPreserveKeys()
    {
        $compact = (new Underscore([0, 1, false, 2, "", 3,]))->compact(true);
        $this->assertEquals([1 => 1, 3 => 2, 5 => 3,], $compact->toArray());
    }

    public function testIsFalsyTrue()
    {
        $this->assertTrue(Underscore::isFalsy(0));
        $this->assertTrue(Underscore::isFalsy(""));
        $this->assertTrue(Underscore::isFalsy(false));
        $this->assertTrue(Underscore::isFalsy(null));
        $this->assertTrue(Underscore::isFalsy(NAN));
    }

    public function testFalsyFalse()
    {
        $this->assertFalse(Underscore::isFalsy(5));
    }

    public function testFlatten()
    {
        $flatten = $this->stooges->flatten();
        $this->assertEquals(['moe', 40, 'larry', 50, 'curly', 60,], $flatten->toArray());
    }

    public function testFlattenPreserveKeys()
    {
        $flatten = $this->stooges->flatten(true);
        $this->assertEquals(["name" => 'curly', "age" => 60,], $flatten->toArray());
    }

    public function testFlattenNested()
    {
        $flatten = (new Underscore([1, [2, [3, 4,]], [[[5,]]],]))->flatten();
        $this->assertEquals([1, 2, 3, 4, 5], $flatten->toArray());
    }

    public function testUnion()
    {
        $underscore1 = new Underscore([1, 2, 3,]);
        $underscore2 = new Underscore([101, 2, 1, 10,]);
        $union1 = $underscore1->union($underscore2);
        $union2 = $underscore2->union($underscore1);

        $this->assertEquals([1, 2, 3, 101, 6 => 10,], $union1->toArray());
        $this->assertEquals([101, 2, 1, 10, 6 => 3,], $union2->toArray());
    }

    public function testIntersect()
    {
        $underscore1 = new Underscore([1, 2, 3,]);
        $underscore2 = new Underscore([101, 2, 1, 10,]);
        $intersect = $underscore1->intersection($underscore2);

        $this->assertEquals([1, 2,], $intersect->toArray());
    }

    public function testDifference()
    {
        $underscore1 = new Underscore([1, 12, 2, 3,]);
        $underscore2 = new Underscore([101, 2, 1, 10,]);
        $difference = $underscore1->difference($underscore2);

        $this->assertEquals([1 => 12, 3 => 3,], $difference->toArray());
    }

    public function testUnique()
    {
        $underscore = new Underscore([1, 2, 3, 1, 5, 8, 4, 2, 3,]);
        $unique = $underscore->unique();

        $this->assertEquals([1, 2, 3, 4 => 5, 8, 4,], $unique->toArray());
    }

    public function testZip()
    {
        $underscore1 = new Underscore([1, 12, 2, 3,]);
        $underscore2 = new Underscore([101, 2, 1, 10,]);
        $difference = $underscore1->zip($underscore2);

        $this->assertEquals([[1, 101,], [12, 2,], [2, 1,], [3, 10,],], $difference->toArray());
    }

    public function testIndexOf()
    {
        $underscore = new Underscore([1, 12, 2, 3,]);
        $index = $underscore->indexOf(12);

        $this->assertEquals(1, $index);
    }

    public function testIndexOfNotExist()
    {
        $underscore = new Underscore([1, 12, 2, 3,]);
        $index = $underscore->indexOf(10);

        $this->assertEquals(-1, $index);
    }

    public function testLastIndexOf()
    {
        $underscore = new Underscore([1, 12, 2, 12, 3,]);
        $index = $underscore->lastIndexOf(12);

        $this->assertEquals(3, $index);
    }

    public function testLastIndexOfNotExist()
    {
        $underscore = new Underscore([1, 12, 2, 3,]);
        $index = $underscore->lastIndexOf(10);

        $this->assertEquals(-1, $index);
    }
}