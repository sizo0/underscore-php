<?php
namespace Underscore;

/**
 * Class Underscore
 *
 * @package Underscore
 * @author Hassan El Omari Alaoui <elomarialaoui.hassan@gmail.com>
 */
class Underscore
{
    /**
     * Wrapped array
     *
     * @var array
     */
    private $array;

    /**
     * Falsy values
     *
     * @var array
     */
    private static $falsy = ["", false, null, NAN, 0,];

    /**
     * Constructor
     *
     * @param Underscore|array|null $subject
     * @throws \Exception
     */
    public function __construct($subject = null)
    {
        if (is_null($subject)) {
            $subject = [];
        }

        if ($subject instanceof Underscore) {
            $subject = $subject->toArray();
        }

        if (!is_array($subject)) {
            throw new \Exception("Parameter must be an instance of '\\Underscore\\Underscore', an 'array' or 'null'");
        }

        $subject = $this->_constructRecursive($subject);

        $this->array = $subject;
    }

    /**
     * Converts all the arrays contained
     * in subject to an Underscore object
     *
     * @param array $subject
     * @return array
     */
    private function _constructRecursive(array $subject)
    {
        $converted = [];
        foreach ($subject as $key => $value) {
            if (is_array($value)) {
                $value = new static($value);
            }
            $converted[$key] = $value;
        }
        return $converted;
    }

    /**
     * Construct a new object from array or self
     *
     * @param Underscore|array|null $subject
     * @return Underscore
     */
    public static function _($subject = null)
    {
        return new static($subject);
    }

    /**
     * Returns wrapped array
     *
     * Convert to array all
     * the nested Underscore objects
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        $this->each(function ($value, $key) use (&$array) {
            if ($value instanceof Underscore) {
                $value = $value->toArray();
            }
            $array[$key] = $value;
        });
        return $array;
    }

    /**
     * Accessor for array
     *
     * @return array
     */
    public function getArray()
    {
        return $this->array;
    }

    /**
     * Reverses the array
     *
     * @param bool $preserve_keys True to preserve numeric keys, false otherwise
     * @return Underscore
     */
    public function reverse($preserve_keys = false)
    {
        return new static(array_reverse($this->array, $preserve_keys));
    }

    /**
     * Pushes a pair key value into the array
     *
     * If the value is null, the key is considered
     * as a value
     *
     * @param mixed $key key
     * @param mixed $value value
     * @return Underscore
     */
    public function push($key, $value = null)
    {
        if (is_null($value)) {
            $this->array[] = $key;
        } else {
            $this->array[$key] = $value;
        }
        return $this;
    }

    /**
     * Gets a value from the array using its key
     *
     * Returns null if the key does not exist
     *
     * @param int|string $key
     * @return mixed|null
     * @throws \Exception
     */
    public function get($key)
    {
        if (!(is_string($key) || is_int($key))) {
            throw new \Exception("Parameter must a 'string' or an 'integer'");
        }

        return array_key_exists($key, $this->array) ? $this->array[$key] : null; // TODO: make a method exists_key, and exists (for values)
    }

    /**
     * Returns the first value of the array
     *
     * @param int $count
     * @return Underscore|mixed
     * @throws \Exception
     */
    public function first($count = 1)
    {
        if (!is_numeric($count)) {
            throw new \Exception("Parameter must be an 'integer'");
        }

        $count = $count > $this->size() ? $this->size() : $count;
        $count = $count <= 0 ? 1 : $count;

        $underscore = new static(array_slice($this->array, 0, $count));

        return $underscore->size() == 1 ? $underscore->get(0) : $underscore;
    }

    /**
     * Returns the size of the array
     *
     * @return int
     */
    public function size()
    {
        return count($this->array);
    }

    /**
     * Iterates over array of elements
     * starting from the first one
     *
     * Breaks the loop by returning true
     *
     * @param Callable $cb callback, arguments are the value and the key
     * @return Underscore
     */
    public function each(Callable $cb)
    {
        foreach ($this->array as $key => $value) {
            $break = $cb($value, $key);
            if ($break) {
                break;
            }
        }
        return $this;
    }

    /**
     * Produces a new array by iterating over
     * the array and applying the callback to
     * each element
     *
     * @param Callable $cb callback, takes value and key as arguments
     * @return Underscore
     */
    public function map(Callable $cb)
    {
        $mapped_array = [];
        $this->each(function ($value, $key) use ($cb, &$mapped_array) {
            $mapped_array[$key] = $cb($value, $key);
        });
        return new static($mapped_array);
    }

    /**
     * Reduces the array of elements
     * starting from the beginning
     * into a single mixed element
     *
     * @param Callable $cb callback, takes accumulator and value as arguments
     * @param mixed $init value to initialize
     * @return mixed
     */
    public function fold_left(Callable $cb, $init)
    {
        $acc = $init;
        $this->each(function ($value, $key) use ($cb, &$acc) {
            $acc = $cb($acc, $value, $key);
        });
        return $acc;
    }

    /**
     * Reduces the array of elements
     * starting from the end
     * into a single mixed element
     *
     * @param Callable $cb callback, takes accumulator and value as arguments
     * @param mixed $init value to initialize
     * @return mixed
     */
    public function fold_right(Callable $cb, $init)
    {
        return $this->reverse()->fold_left($cb, $init);
    }

    /**
     * Iterates through the array
     * and finds the first value
     * that passes the predicate
     *
     * @param Callable $cb callback, takes value and key as arguments
     * @return mixed
     */
    public function find(Callable $cb)
    {
        $found = null;
        $this->each(function ($value, $key) use ($cb, &$found) {
            if ($cb($value, $key)) {
                $found = $value;
                return true;
            }
        });
        return $found;
    }

    /**
     * Returns an Underscore object
     * of all the elements that pass the predicate $cb
     *
     * @param Callable $cb callback, takes value and key as arguments
     * @return Underscore
     */
    public function filter(Callable $cb)
    {
        $instance = new static();
        $this->each(function ($value, $key) use ($cb, $instance) {
            if ($cb($value, $key)) {
                $instance->push($value);
            }
        });
        return $instance;
    }

    /**
     * Returns all the key-value pairs
     * that contain all the properties
     *
     * @param Underscore|array $properties properties to check
     * @return Underscore
     */
    public function where($properties)
    {
        // $properties = static::_($properties); // TODO: uncomment and manage objetcs (Underscore) in contains by implementing ArrayAccessor and Iterator and adding methods get and set
        return $this->filter(function ($value) use ($properties) {
            $value = static::_($value);
            return $value->contains($properties, true);
        });
    }

    /**
     * Checks if the array comprises the values
     *
     * @param mixed $values values to check
     * @param bool $use_keys check using keys also
     * @return bool
     */
    public function contains($values, $use_keys = false)
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        $contains = false;
        $this->each(function ($v, $k) use (&$values, &$contains, $use_keys) {
            $key = array_search($v, $values); // TODO: make method search
            if ($key !== false && (($v == $values[$key] && !$use_keys) || ($v == $values[$key] && $key === $k))) {
                unset($values[$key]); // TODO: make method delete
                if (count($values) == 0) {
                    $contains = true;
                    return true;
                }
            }
        });
        return $contains;
    }

    /**
     * Returns the first key-value pair
     * that contains all the properties
     *
     * Stops looping as soon as a pair is found
     *
     * @param Underscore|array $properties properties to check
     * @return Underscore|null
     */
    public function findWhere($properties)
    {
        return $this->find(function ($value) use ($properties) {
            $value = static::_($value);
            return $value->contains($properties, true);
        });
    }

    /**
     * Returns an Underscore object
     * of all the elements that do not pass the predicate $cb
     *
     * @param Callable $cb callback, takes value and key as arguments
     * @return Underscore
     */
    public function reject(Callable $cb)
    {
        return $this->filter(function ($value, $key) use ($cb) {
            return !$cb($value, $key);
        });
    }

    /**
     * Returns true if all of the values in the array
     * pass the predicate, false otherwise
     *
     * @param Callable $cb callback, takes value and key as arguments
     * @return bool
     */
    public function every(Callable $cb)
    {
        return $this->find(function ($value, $key) use ($cb) {
            return !$cb($value, $key);
        }) == null;
    }

    /**
     * Returns true if at least one of the values in
     * the array passes the predicate, false otherwise
     *
     * @param Callable $cb callback, takes value and key as arguments
     * @return bool
     */
    public function some(Callable $cb)
    {
        return $this->find(function ($value, $key) use ($cb) {
            return $cb($value, $key);
        }) != null;
    }

    /**
     * Extracts the values of the key passed
     * in parameter
     *
     * @param int|string $key
     * @return Underscore
     */
    public function pluck($key)
    {
        return $this->map(function (Underscore $value) use ($key) {
            return $value->get($key); // TODO: change to $value->$key by implementing the magic method
        });
    }

    /**
     * Returns the value that always passes
     * the callback $compare
     *
     * The callback $cb generates the criterion
     * by which the values are compared
     *
     * Returns null if the array is empty
     *
     * @param Callable $compare takes two arguments
     * @param Callable $cb takes value as argument
     * @return mixed|null
     */
    protected function compare(Callable $compare, Callable $cb = null)
    {
        if ($cb == null) {
            $cb = function ($value) {
                return $value;
            };
        }

        $comp = null;
        $this->each(function ($value) use ($cb, $compare, &$comp) {
            if (is_null($comp) || $compare($cb($value), $cb($comp))) {
                $comp = $value;
            }
        });
        return $comp;
    }

    /**
     * Returns the maximum value in the array
     *
     * The callback function generates the criterion
     * by which the values are compared
     *
     * Returns null if the array is empty
     *
     * @param Callable $cb callback, takes value as argument
     * @return mixed|null
     */
    public function max(Callable $cb = null)
    {
        return $this->compare(function ($a, $b) {
            return $a > $b;
        }, $cb);
    }

    /**
     * Returns the minimum value in the array
     *
     * The callback function generates the criterion
     * by which the values are compared
     *
     * Returns null if the array is empty
     *
     * @param Callable $cb callback, takes value as argument
     * @return mixed|null
     */
    public function min(Callable $cb = null)
    {
        return $this->compare(function ($a, $b) {
            return $a < $b;
        }, $cb);
    }

    /**
     * Sorts the array using whether a callable $cb
     * or a string that defines a key
     *
     * @param Callable|string $cb takes value as argument
     * @return Underscore
     * @throws \Exception
     */
    public function sortBy($cb)
    {
        if (is_string($cb)) {
            $cb = function (array $value) use ($cb) {
                return $value[$cb];
            };
        }

        if (!is_callable($cb)) {
            throw new \Exception("The parameter must be a 'string' or a 'callable'");
        }

        $array = $this->toArray();
        usort($array, function ($a, $b) use ($cb) {
            if ($cb($a) == $cb($b)) {
                return 0;
            }
            return $cb($a) < $cb($b) ? -1 : 1;
        });

        return new static($array);
    }

    /**
     * Groups values by a criterion defined by
     * the callback
     *
     * @param Callable $cb callback, takes value as argument
     * @return Underscore
     */
    public function groupBy(Callable $cb)
    {
        $groupBy = $this->fold_left(function ($acc, $value) use ($cb) {
            $k = $cb($value);
            if (!array_key_exists($k, $acc)) {
                $acc[$k] = [];
            }
            $acc[$k][] = $value;
            return $acc;
        }, []);

        return new static($groupBy);
    }

    /**
     * Same as groupBy except that the keys are unique
     *
     * If two values have the same key, the first one is chosen
     *
     * @param Callable $cb callback, takes value as argument
     * @return Underscore
     */
    public function indexBy(Callable $cb)
    {
        return $this->groupBy($cb)->map(function (Underscore $value) {
            return $value->first();
        });
    }

    /**
     * Same as groupBy except that it returns the
     * number of elements instead of an array of elements
     *
     * @param Callable $cb takes values as argument
     * @return Underscore
     */
    public function countBy(Callable $cb)
    {
        return $this->groupBy($cb)->map(function (Underscore $value) {
            return $value->size();
        });
    }

    /**
     * Returns a shuffled copy of the array
     *
     * @return Underscore
     */
    public function shuffle()
    {
        $array = $this->toArray();
        shuffle($array);
        return new static($array);
    }

    /**
     * Returns an object of 'count' random values
     * from the array
     *
     * @param int|null $count
     * @return Underscore
     * @throws \Exception
     */
    public function sample($count = 1)
    {
        // TODO: choose values of $this

        if (!is_numeric($count)) {
            throw new \Exception("Parameter must be an 'integer'");
        }

        $count = $count > $this->size() ? $this->size() : $count;
        $count = $count <= 0 ? 1 : $count;

        $underscore = new static();

        $chosen = [];
        for ($i = 0; $i < $count; $i++) {
            do {
                $rand = rand(0, $this->size() - 1);
            } while (in_array($rand, $chosen));
            $chosen[] = $rand;
            $value = $this->get($rand);
            $underscore->push($value);
        }

        return $underscore;
    }

    /**
     * Divides the array into two arrays where
     * the first one passes the callback,
     * while the other does not
     *
     * @param Callable $cb takes value as argument
     * @return static
     */
    public function partition(Callable $cb)
    {
        $partition = $this->fold_left(function ($acc, $value) use ($cb) {
            if ($cb($value)) {
                $acc[0][] = $value;
            } else {
                $acc[1][] = $value;
            }
            return $acc;
        }, []);

        return new static($partition);
    }

    /**
     * Excludes the last 'count' values from the array
     *
     * @param int $count
     * @return static
     * @throws \Exception
     */
    public function initial($count = 1)
    {
        // TODO: create method to put this first part in it

        if (!is_numeric($count)) {
            throw new \Exception("Parameter must be an 'integer'");
        }

        $count = $count > $this->size() ? $this->size() : $count;
        $count = $count <= 0 ? 1 : $count;
        $count = $this->size() - $count;

        return new static(array_slice($this->array, 0, $count, true));
    }

    /**
     * Returns the last 'count' values from the array
     *
     * @param int $count
     * @return Underscore|mixed
     * @throws \Exception
     */
    public function last($count = 1)
    {
        if (!is_numeric($count)) {
            throw new \Exception("Parameter must be an 'integer'");
        }

        $count = $count > $this->size() ? $this->size() : $count;
        $count = $count <= 0 ? 1 : $count;
        $count = -$count;

        $underscore = $this->rest($count);

        return $underscore->size() == 1 ? $underscore->get(0) : $underscore;
    }

    /**
     * Returns the rest of the elements of
     * the array
     *
     * @param int $index
     * @return Underscore
     */
    public function rest($index = 1)
    {
        return new static(array_slice($this->array, $index));
    }

    /**
     * Removes falsy values
     *
     * @see isFalsy
     * @param bool $preserve_keys True to preserve keys
     * @return Underscore
     */
    public function compact($preserve_keys = false)
    {
        return $this->fold_left(function (Underscore $acc, $value, $key) use ($preserve_keys) {
            if (!static::isFalsy($value)) {
                if ($preserve_keys) {
                    $acc->push($key, $value);
                } else {
                    $acc->push($value);
                }
            }
            return $acc;
        }, new Underscore());
    }

    /**
     * Returns true if value is falsy
     *
     * @param mixed $value
     * @return bool
     */
    public static function isFalsy($value)
    {
        return in_array($value, static::$falsy) || is_nan($value); // in_array for NAN not working
    }

    /**
     * Flattens the array
     *
     * Note that if preserve keys is true and
     * two or more values have the same key,
     * the last value will be preserved
     *
     * @param bool $preserve_keys preserve keys
     * @return Underscore
     */
    public function flatten($preserve_keys = false)
    {
        return $this->fold_left(function (Underscore $acc, $value, $key) use ($preserve_keys) {
            if ($value instanceof Underscore) {
                $value = $value->flatten($preserve_keys);
                $value->each(function($value, $key) use (&$acc, $preserve_keys) {
                    if ($preserve_keys) {
                        $acc->push($key, $value);
                    } else {
                        $acc->push($value);
                    }
                });
            } else {
                if ($preserve_keys) {
                    $acc->push($key, $value);
                } else {
                    $acc->push($value);
                }
            }
            return $acc;
        }, new Underscore());
    }

    /**
     * Union between this and the Underscore
     * object passed as parameter
     *
     * Keys are preserved
     *
     * @param Underscore $underscore
     * @return Underscore
     */
    public function union(Underscore $underscore)
    {
        return (new Underscore(array_merge($this->toArray(), $underscore->toArray())))->unique(); // TODO: add merge method
    }

    /**
     * Returns the intersection between
     * two Underscore objects
     *
     * Keys of the first occurrences are preserved
     *
     * @param Underscore $underscore
     * @return Underscore
     */
    public function intersection(Underscore $underscore)
    {
        return new Underscore(array_intersect($this->toArray(), $underscore->toArray()));
    }

    /**
     * Returns the difference between
     * two Underscore objects
     *
     * Keys of the first occurrences are preserved
     *
     * @param Underscore $underscore
     * @return Underscore
     */
    public function difference(Underscore $underscore)
    {
        return new Underscore(array_diff($this->toArray(), $underscore->toArray()));
    }

    /**
     * Only all the first occurrences are kept.
     * Equality is tested by ===
     *
     * Keys are preserved
     *
     * @return Underscore
     */
    public function unique()
    {
        return new Underscore(array_unique($this->array));
    }

    /**
     * Merges together the values of each of the arrays
     * with the values at the corresponding position.
     *
     * @param Underscore $underscore
     * @return Underscore
     */
    public function zip(Underscore $underscore)
    {
        return new Underscore(array_map(null, $this->toArray(), $underscore->toArray()));
    }

    /**
     * Returns the index of the value
     * passed as a parameter
     *
     * Returns -1 if the value is not found
     *
     * @param mixed $value
     * @return int
     */
    public function indexOf($value)
    {
        return array_search($value, $this->array) ?: -1;
    }


    /**
     * Returns the last index of the value
     * passed as a parameter
     *
     * Returns -1 if the value is not found
     *
     * @param mixed $value
     * @return int
     */
    public function lastIndexOf($value)
    {
        return $this->reverse(true)->indexOf($value);
    }
}